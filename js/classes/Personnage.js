
function Personnage(url_image) {
	this.image = new Image();
	this.image.referenceDuPerssonnage = this;
	this.image.onload = function() {
		if(!this.complete) 
			throw new Error("Erreur de chargement du tileset");
	}
	this.image.src = "img/" + url_image;
	//this.x = x
	//this.y = y
}

Personnage.prototype.dessiner = function(context, x, y) {
	
	context.drawImage(this.image, x * 32, y * 32, 32, 32);
}

